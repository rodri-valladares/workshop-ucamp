const container = document.querySelector('.container');
const asientos = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');  

populateUI();

let ticketPrecio = parseInt( movieSelect.value);
//Guarda el indice y el precio de la pelicula seleccionada
function setPeliculaData(movieIndex, peliculaPrecio){
  localStorage.setItem('IndicePeliculaSeleccionada', movieIndex);
  localStorage.setItem('PrecioPeliculaSeleccionada', peliculaPrecio);
}

function updateSelectedCount(){
  const seleccionadoAsientos = document.querySelectorAll('.row .seat.selected');
  //console.log(seleccionadoAsientos);

  //CREO un array nuevo 
  const asientosIndice = [...seleccionadoAsientos].map((asientoElemento)=>[...asientos].indexOf(asientoElemento) )
  console.log(asientosIndice);

  localStorage.setItem('sleccionadoAsientos', JSON.stringify(asientosIndice));

  const seleccionadoAsientosCount = seleccionadoAsientos.length;
  count.innerText = seleccionadoAsientosCount;
  total.innerText = seleccionadoAsientosCount * ticketPrecio;

}

//Obtener información del localstorage y pintar UI
function populateUI(){
  const seleccionadoAsientos = JSON.parse(localStorage.getItem('sleccionadoAsientos'));
  if(seleccionadoAsientos !==null && seleccionadoAsientos.length > 0){
    asientos.forEach((seat,index)=>{
      //console.log("asiento: ", seat);
      //console.log("indice: ", index);
      if(seleccionadoAsientos.indexOf(index) > -1){
        seat.classList.add('selected');
      }
    });
  }

}

movieSelect.addEventListener('change',(e)=>{
  ticketPrecio = parseInt(e.target.value);
  
  //console.log(e.target.selectedIndex, e.target.value);

  //llamo a la función que guarda indice y precio
  setPeliculaData(e.target.selectedIndex, e.target.value);

});

//Evento de click en los asientos 
container.addEventListener('click',(e)=>{
  if(e.target.classList.contains('seat') && !e.target.classList.contains('occupied')){
    e.target.classList.toggle('selected');
  
    //Llamos a función que calcula total segun asientos seleccionados por
    updateSelectedCount();
  }
});

updateSelectedCount();